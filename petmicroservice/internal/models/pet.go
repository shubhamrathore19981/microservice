package models

type Category struct {
	ID   uint   `json:"id" gorm:"primaryKey"`
	Name string `json:"name" gorm:"type:varchar(100)"`
}

type Tag struct {
	ID   uint   `json:"id" gorm:"primaryKey"`
	Name string `json:"name" gorm:"type:varchar(100)"`
}

type Pet struct {
	ID         uint     `json:"id" gorm:"primaryKey"`
	Name       string   `json:"name" gorm:"type:varchar(100)"`
	CategoryID uint     `json:"category_id" gorm:"not null"`
	Category   Category `json:"category" gorm:"foreignKey:CategoryID"`
	PhotoUrls  string   `json:"photo_urls" gorm:"type:varchar(100)"`
	Tags       []Tag    `json:"tags" gorm:"many2many:pet_tags"`
	Status     string   `json:"status" gorm:"type:varchar(20)"`
}
