package handlers

import (
	"encoding/json"
	"net/http"
	"strconv"

	"github.com/gorilla/mux"
	"github.com/labstack/gommon/log"
	"github.com/prometheus/client_golang/prometheus"
	"github.com/prometheus/client_golang/prometheus/promauto"
	"veltris.com/internal/models"
	"veltris.com/internal/services"
)

type PetHandler struct {
	StorageService *services.StorageService
}

var (
	petsCreatedTotal = promauto.NewCounter(prometheus.CounterOpts{
		Name: "pets_created_total",
		Help: "Total number of pets created",
	})
	petCategoriesTotal = promauto.NewCounterVec(prometheus.CounterOpts{
		Name: "pet_categories_total",
		Help: "Total number of pets per category",
	}, []string{"category"})
	petTagsTotal = promauto.NewCounterVec(prometheus.CounterOpts{
		Name: "pet_tags_total",
		Help: "Total number of pets per tag",
	}, []string{"tag"})
)

func (h *PetHandler) GetPets(w http.ResponseWriter, r *http.Request) {
	pets, err := h.StorageService.GetPets()
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
	log.Info("Successfully Triggred Get Request")
	json.NewEncoder(w).Encode(pets)
}

func (h *PetHandler) Liveness(w http.ResponseWriter, r *http.Request) {
	w.WriteHeader(http.StatusOK)
}

func (h *PetHandler) Readiness(w http.ResponseWriter, r *http.Request) {
	w.WriteHeader(http.StatusOK)
}

func (h *PetHandler) CreatePet(w http.ResponseWriter, r *http.Request) {
	var pet models.Pet
	if err := json.NewDecoder(r.Body).Decode(&pet); err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}
	err := h.StorageService.CreatePet(pet)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
	//update metric
	petsCreatedTotal.Inc()
	petCategoriesTotal.WithLabelValues(pet.Category.Name).Inc()

	for _, tag := range pet.Tags {
		petTagsTotal.WithLabelValues(tag.Name)
	}
	log.Info("Successfully added Pet to the DB and Promethoeus")
	w.WriteHeader(http.StatusCreated)
	json.NewEncoder(w).Encode(pet)
}

func (h *PetHandler) GetPetByID(w http.ResponseWriter, r *http.Request) {
	params := mux.Vars(r)
	id, _ := strconv.Atoi(params["id"])
	pet, err := h.StorageService.GetPetByID(uint(id))
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
	log.Info("Successfully Triggred Get Request By Id")
	json.NewEncoder(w).Encode(pet)
}

func (h *PetHandler) DeletePetByID(w http.ResponseWriter, r *http.Request) {
	params := mux.Vars(r)
	id, _ := strconv.Atoi(params["id"])
	err := h.StorageService.DeletePetByID(uint(id))
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
	log.Info("Successfully Deleted Pets From DB Request")
	w.WriteHeader(http.StatusNoContent)
}
