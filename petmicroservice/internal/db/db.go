package db

import (
	"fmt"

	"github.com/labstack/gommon/log"
	"gorm.io/driver/postgres"
	"gorm.io/gorm"
	"veltris.com/internal/models"
)

const (
	// host = "192.168.174.67"
	host = "postgresql"
	// host     = "localhost"
	port     = 5432
	user     = "postgres"
	password = "hbZ5XlGcV2"
	dbname   = "newpetservice"
)

var DB *gorm.DB

func Init() {
	psqlInfo := fmt.Sprintf("host=%s port=%d user=%s "+"password=%s dbname=%s sslmode=disable", host, port, user, password, dbname)
	log.Info("init db...")
	var err error
	DB, err = gorm.Open(postgres.Open(psqlInfo), &gorm.Config{})
	if err != nil {
		log.Panic("failed to connect database")
	}
	DB.Migrator().DropTable(&models.Pet{}, &models.Category{}, &models.Tag{})

	// Auto-migrate the schema
	err = DB.AutoMigrate(&models.Pet{}, &models.Category{}, &models.Tag{})
	if err != nil {
		log.Errorf("db error := ", err)
	}
	log.Info("Successfully Connected to db ...")
}
