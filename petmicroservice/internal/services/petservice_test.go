package services_test

import (
	"testing"

	"github.com/onsi/ginkgo"
	"github.com/onsi/gomega"
	"gorm.io/driver/sqlite"
	"gorm.io/gorm"
	"veltris.com/internal/models"
	"veltris.com/internal/services"
)

func TestStorageService(t *testing.T) {
	gomega.RegisterFailHandler(ginkgo.Fail)
	ginkgo.RunSpecs(t, "StorageService Suite")
}

var _ = ginkgo.Describe("StorageService", func() {
	var (
		db             *gorm.DB
		storageService *services.StorageService
	)

	ginkgo.BeforeSuite(func() {
		var err error
		db, err = gorm.Open(sqlite.Open("test.db"), &gorm.Config{})
		gomega.Expect(err).NotTo(gomega.HaveOccurred())
		db.AutoMigrate(&models.Pet{}, &models.Category{}, &models.Tag{})
		storageService = services.NewStorageService(db)
	})

	ginkgo.AfterSuite(func() {
		sqlDB, err := db.DB()
		gomega.Expect(err).NotTo(gomega.HaveOccurred())
		sqlDB.Close()
	})

	ginkgo.BeforeEach(func() {
		db.Exec("DELETE FROM pets")
		db.Exec("DELETE FROM categories")
		db.Exec("DELETE FROM tags")
	})

	ginkgo.Describe("GetPets", func() {
		ginkgo.It("should return all pets", func() {
			cat := models.Category{ID: 1, Name: "Cats"}
			pet := models.Pet{
				ID:        1,
				Name:      "Whiskers",
				Category:  cat,
				PhotoUrls: "url1",
				Tags:      []models.Tag{{ID: 1, Name: "tag1"}},
				Status:    "available",
			}
			db.Create(&cat)
			db.Create(&pet)

			pets, err := storageService.GetPets()
			gomega.Expect(err).NotTo(gomega.HaveOccurred())
			gomega.Expect(pets).To(gomega.HaveLen(1))
			gomega.Expect(pets[0].Name).To(gomega.Equal("Whiskers"))
		})
	})

	ginkgo.Describe("CreatePet", func() {
		ginkgo.It("should create a new pet", func() {
			cat := models.Category{ID: 1, Name: "Cats"}
			db.Create(&cat)
			pet := models.Pet{
				Name:      "Whiskers",
				Category:  cat,
				PhotoUrls: "url1",
				Tags:      []models.Tag{{ID: 1, Name: "tag1"}},
				Status:    "available",
			}

			err := storageService.CreatePet(pet)
			gomega.Expect(err).NotTo(gomega.HaveOccurred())

			var pets []models.Pet
			db.Find(&pets)
			gomega.Expect(pets).To(gomega.HaveLen(1))
			gomega.Expect(pets[0].Name).To(gomega.Equal("Whiskers"))
		})
	})

	ginkgo.Describe("GetPetByID", func() {
		ginkgo.It("should return the pet by ID", func() {
			cat := models.Category{ID: 1, Name: "Cats"}
			pet := models.Pet{
				ID:        1,
				Name:      "Whiskers",
				Category:  cat,
				PhotoUrls: "url1",
				Tags:      []models.Tag{{ID: 1, Name: "tag1"}},
				Status:    "available",
			}
			db.Create(&cat)
			db.Create(&pet)

			returnedPet, err := storageService.GetPetByID(1)
			gomega.Expect(err).NotTo(gomega.HaveOccurred())
			gomega.Expect(returnedPet.Name).To(gomega.Equal("Whiskers"))
		})
	})

	ginkgo.Describe("DeletePetByID", func() {
		ginkgo.It("should delete the pet by ID", func() {
			cat := models.Category{ID: 1, Name: "Cats"}
			pet := models.Pet{
				ID:        1,
				Name:      "Whiskers",
				Category:  cat,
				PhotoUrls: "url1",
				Tags:      []models.Tag{{ID: 1, Name: "tag1"}},
				Status:    "available",
			}
			db.Create(&cat)
			db.Create(&pet)

			err := storageService.DeletePetByID(1)
			gomega.Expect(err).NotTo(gomega.HaveOccurred())

			var pets []models.Pet
			db.Find(&pets)
			gomega.Expect(pets).To(gomega.HaveLen(0))
		})
	})
})
