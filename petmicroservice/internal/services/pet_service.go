package services

import (
	"fmt"

	"github.com/labstack/gommon/log"
	"gorm.io/gorm"
	"veltris.com/internal/models"
)

// Define a struct to encapsulate request and response channels
type StorageService struct {
	DB           *gorm.DB
	PetRequests  chan PetRequest
	PetResponses chan PetResponse
}

// Define request and response types
type PetRequest struct {
	Action string
	Pet    models.Pet
	ID     uint
	RespCh chan PetResponse
}

type PetResponse struct {
	Pets []models.Pet
	Pet  models.Pet
	Err  error
}

func NewStorageService(db *gorm.DB) *StorageService {
	service := &StorageService{
		DB:           db,
		PetRequests:  make(chan PetRequest),
		PetResponses: make(chan PetResponse),
	}
	log.Info("Starting a goroutine to handle channel operations ")
	// Start a goroutine to handle channel operations
	go service.handleRequests()

	return service
}

func (s *StorageService) handleRequests() {
	for req := range s.PetRequests {
		switch req.Action {
		case "GetPets":
			pets, err := s.getPets()
			req.RespCh <- PetResponse{Pets: pets, Err: err}
		case "CreatePet":
			err := s.createPet(req.Pet)
			req.RespCh <- PetResponse{Err: err}
		case "GetPetByID":
			pet, err := s.getPetByID(req.ID)
			req.RespCh <- PetResponse{Pet: pet, Err: err}
		case "DeletePetByID":
			err := s.deletePetByID(req.ID)
			req.RespCh <- PetResponse{Err: err}
		}
	}
}

// Internal methods that interact with the database
func (s *StorageService) getPets() ([]models.Pet, error) {
	var pets []models.Pet
	result := s.DB.Preload("Category").Preload("Tags").Find(&pets)
	return pets, result.Error
}

func (s *StorageService) createPet(pet models.Pet) error {
	fmt.Println("going to insert into db", pet)
	// pet.PhotoUrls = "url1"
	result := s.DB.Create(&pet)
	fmt.Println("create error: ", result.Error)
	return result.Error
}

func (s *StorageService) getPetByID(id uint) (models.Pet, error) {
	var pet models.Pet
	result := s.DB.Preload("Category").Preload("Tags").First(&pet, id)
	return pet, result.Error
}

func (s *StorageService) deletePetByID(id uint) error {
	result := s.DB.Delete(&models.Pet{}, id)
	return result.Error
}

// Public methods that use channels to handle requests
func (s *StorageService) GetPets() ([]models.Pet, error) {
	respCh := make(chan PetResponse)
	s.PetRequests <- PetRequest{Action: "GetPets", RespCh: respCh}
	resp := <-respCh
	return resp.Pets, resp.Err
}

func (s *StorageService) CreatePet(pet models.Pet) error {
	respCh := make(chan PetResponse)
	s.PetRequests <- PetRequest{Action: "CreatePet", Pet: pet, RespCh: respCh}
	resp := <-respCh
	return resp.Err
}

func (s *StorageService) GetPetByID(id uint) (models.Pet, error) {
	respCh := make(chan PetResponse)
	s.PetRequests <- PetRequest{Action: "GetPetByID", ID: id, RespCh: respCh}
	resp := <-respCh
	return resp.Pet, resp.Err
}

func (s *StorageService) DeletePetByID(id uint) error {
	respCh := make(chan PetResponse)
	s.PetRequests <- PetRequest{Action: "DeletePetByID", ID: id, RespCh: respCh}
	resp := <-respCh
	return resp.Err
}
