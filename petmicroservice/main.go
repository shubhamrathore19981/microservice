package main

import (
	"net/http"

	"github.com/gorilla/mux"
	"github.com/labstack/gommon/log"
	"github.com/prometheus/client_golang/prometheus/promhttp"
	"veltris.com/internal/db"
	"veltris.com/internal/handlers"
	"veltris.com/internal/services"
)

func main() {
	log.Info("Starting Pet Service...")
	db.Init()
	storageService := services.NewStorageService(db.DB)
	petHandler := &handlers.PetHandler{StorageService: storageService}
	router := mux.NewRouter()
	router.Handle("/metrics", promhttp.Handler())
	router.HandleFunc("/pets", petHandler.GetPets).Methods("GET")
	router.HandleFunc("/Liveness", petHandler.Liveness).Methods("GET")
	router.HandleFunc("/Readiness", petHandler.Readiness).Methods("GET")
	router.HandleFunc("/pet", petHandler.CreatePet).Methods("POST")
	router.HandleFunc("/pet/{id}", petHandler.GetPetByID).Methods("GET")
	router.HandleFunc("/pet/{id}", petHandler.DeletePetByID).Methods("DELETE")
	http.ListenAndServe(":8081", router)
}
