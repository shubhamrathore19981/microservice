## Topology
![Topology](petmicroservice/docs/topology.png)

## Pre-requisite
1. Install golang v1.11 or above.
2. Basic understanding of the golang syntax.
3. Basic understanding of SQL query.
4. Code Editor (I recommend to use VS Code with Go extension by Microsoft installed)
5. Postman for calling APIs
6. K8s cluster

## Deployment

```bash
    brew install helm #[mac os]
    helm install postgresql oci://registry-1.docker.io/bitnamicharts/postgresql
    export POSTGRES_PASSWORD=$(kubectl get secret --namespace kubernetes-dashboard postgresql -o jsonpath="{.data.postgres-password}" | base64 -d)
    echo $POSTGRES_PASSWORD #replace  this password with password = "hbZ5XlGcV2" in db.go
    #build the docker image 
    git clone https://gitlab.com/shubhamrathore19981/microservice.git
    cd microservice/petmicroservice
    #you can use existing image also but if you want some code change then you have to update the image with whatever build
    docker build -t yourdockerid/petmicroservice:v1 .
    docker push yourdockerid/petmicroservice:v1 
    helm install petservice  ./cmd/petservicehelm  #update value.go with above build image
    kubectl kubectl port-forward --address 0.0.0.0 svc/petservice  8081:8081 
    kubectl logs podname
    #now if all this setup brings up you can trigger apis from postmen or curl
```
 ### Postgres installation & update password
```
To deploy this project run
```
![pwdchange](images/postgres.png)
```
Deployment through helm
```
![deployment](petmicroservice/docs/helminstall.png)
```
```
## API Reference

#### Get all pets

```http
  GET http://127.0.0.1:8081/pets
```
   ## Get method
   ![Get](petmicroservice/docs/getallreq.png)
    
#### Get pets by id

```http
  GET http://127.0.0.1:8081/pets/${id}
```

| Parameter | Type     | Description                       |
| :-------- | :------- | :-------------------------------- |
| `id`      | `int` | **Required**. Id of pets to fetch |


![GetbyId](petmicroservice/docs/getbyid.png)

#### Post Pets
```http
  POST http://127.0.0.1:8081/pet
```
| Parameter | Type     | Description{Body}                       |
| :-------- | :------- | :-------------------------------- |
| `body`    | `Json` |`` |

![Post](petmicroservice/docs/posreq.png)

#### Delete Pet by id
```http 
  DELETE http://127.0.0.1:8081/pet/${id}
```
![Delete](petmicroservice/docs/deletebyid.png)


## Authors

- [@shubhamrathore19981]




 
  



